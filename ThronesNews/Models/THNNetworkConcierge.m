//
//  THNNetworkConcierge.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNNetworkConcierge.h"
#import "THNURLConcierge.h"
#import "THNArticlesParser.h"

@interface THNNetworkConcierge ()

@property (nonatomic) NSURLSession *session;
@property (nonatomic) THNURLConcierge *urlsModel;
@property (nonatomic) NSURLSessionDataTask *popularArticlesTask;
@property (nonatomic, copy) void (^popularArticlesCompletion)(NSArray <THNArticle *>*);

@end

@implementation THNNetworkConcierge

#pragma mark - Public

@synthesize popularArticlesCompletion;

+ (THNNetworkConcierge *)sharedInstance
{
    static id sharedInstance;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });

    return sharedInstance;
}

- (void)fetchMostPopularArticlesWithCompletion:(void (^)(NSArray <THNArticle *>*news))completion
{
    if (self.popularArticlesTask &&
        self.popularArticlesTask.state == NSURLSessionTaskStateRunning)
    {
        return;
    }

    self.popularArticlesCompletion = completion;
    self.popularArticlesTask = [self.session dataTaskWithURL:self.urlsModel.charactersNewsURL completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        THNArticlesParser *parser = [[THNArticlesParser alloc] init];

        /* By default NSURLSession works on a background thread
         * that's why we need to call completionBlock on main thread
         * so user of this completion block can use it in 
         * UI-related code
         */
        dispatch_async(dispatch_get_main_queue(), ^{
            self.popularArticlesCompletion([parser parseArticlesWithData:data]);
        });
    }];

    [self.popularArticlesTask resume];
}

#pragma mark - Private

- (instancetype)init
{
    self = [super init];

    if (self)
    {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        _session = [NSURLSession sessionWithConfiguration:config
                                                 delegate:nil
                                            delegateQueue:nil];
        _urlsModel = [[THNURLConcierge alloc] init];
    }

    return self;
}

@end
