//
//  THNArticlesParser.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "THNArticle.h"

@interface THNArticlesParser : NSObject

- (NSArray <THNArticle *>*)parseArticlesWithData:(NSData *)data;

@end
