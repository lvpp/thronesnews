//
//  THNArticlesProvider.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNArticlesProvider.h"
#import "THNNetworkConcierge.h"
#import <UIKit/UIKit.h>
#import "THNFavouritesConcierge.h"

@interface THNArticlesProvider ()

@property (nonatomic) NSArray <THNArticle *> *articles;
@property (nonatomic) THNFavouritesConcierge *favouritesConcierge;

@end

@implementation THNArticlesProvider

- (instancetype)init
{
    self = [super init];

    if (self)
    {
        _favouritesConcierge = [[THNFavouritesConcierge alloc] init];
        [self startFetchingArticles];
    }
    return self;
}

#pragma mark - Public

- (THNArticle *)articleAtIndexPath:(NSIndexPath *)path
{
    NSUInteger index = (NSUInteger)path.item;

    NSArray *items;

    if (self.state == THNMainModelStateAllArticles)
    {
        items = self.articles;
    }
    else
    {
        items = [self filterFavourites];
    }

    if (index < items.count)
    {
        return items[index];
    }

    return nil;
}

- (NSInteger)numberOfArticles
{
    if (self.state == THNMainModelStateAllArticles)
    {
        return self.articles.count;
    }

    return [[self filterFavourites] count];
}

- (void)favouriteArticleWithID:(NSNumber *)articleID
{
    [self.favouritesConcierge favouriteItem:articleID];
}

- (BOOL)isFavouriteArticleID:(NSNumber *)articleID
{
    return [self.favouritesConcierge doesContainItem:articleID];
}

#pragma mark - Private

- (void)startFetchingArticles
{
    [[THNNetworkConcierge sharedInstance] fetchMostPopularArticlesWithCompletion:^(NSArray *news) {

        /* We do not keep strong pointer
         * to this block, so we can safely
         * use self here
         */
        self.articles = news;

        if ([self.delegate respondsToSelector:@selector(mainModel:didUpdateArticles:)])
        {
            [self.delegate mainModel:self didUpdateArticles:self.articles];
        }
    }];
}

- (NSArray *)filterFavourites
{
    NSArray *allFavourites = [self.favouritesConcierge allIDs];

    NSMutableArray *filteredFavourites = [[NSMutableArray alloc] init];

    for (THNArticle *currentArticle in self.articles)
    {
        if ([allFavourites containsObject:currentArticle.articleID])
        {
            [filteredFavourites addObject:currentArticle];
        }
    }

    return [NSArray arrayWithArray:filteredFavourites];
}

@end
