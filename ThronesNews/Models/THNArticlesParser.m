
//  THNArticlesParser.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNArticlesParser.h"

@implementation THNArticlesParser

- (NSArray <THNArticle *>*)parseArticlesWithData:(NSData *)data
{
    if (!data)
    {
        return nil;
    }

    NSDictionary *json  = [NSJSONSerialization JSONObjectWithData:data
                                                          options:kNilOptions
                                                            error:nil];

    NSArray *articlesToParse = json[@"items"];
    NSMutableArray *parsedArticles = [[NSMutableArray alloc] init];

    NSString *basePath = json[@"basepath"];
    for (NSDictionary *singleArticleJSON in articlesToParse)
    {
        NSString *url = [basePath stringByAppendingString:singleArticleJSON[@"url"]];

        THNArticle *article = [[THNArticle alloc] initWithTitle:singleArticleJSON[@"title"]
                                                      thumbnail:singleArticleJSON[@"thumbnail"]
                                                       abstract:singleArticleJSON[@"abstract"]
                                                      articleID:singleArticleJSON[@"id"]
                                                     articleURL:[NSURL URLWithString:url]];
        [parsedArticles addObject:article];
    }

    return [NSArray arrayWithArray:parsedArticles];
}

@end
