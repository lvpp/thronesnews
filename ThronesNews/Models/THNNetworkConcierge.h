//
//  THNNetworkConcierge.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "THNArticle.h"

@interface THNNetworkConcierge : NSObject

+ (THNNetworkConcierge *)sharedInstance;

// TODO: This array can be with generic type: News object
- (void)fetchMostPopularArticlesWithCompletion:(void (^)(NSArray <THNArticle *>*news))completion;

@end
