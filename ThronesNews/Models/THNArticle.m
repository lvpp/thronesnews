//
//  THNArticle.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNArticle.h"

@interface THNArticle ()

@property (nonatomic, readwrite) NSString *title;
@property (nonatomic, readwrite) NSURL *thumbnail;
@property (nonatomic, readwrite) NSString *abstract;
@property (nonatomic, readwrite) NSNumber *articleID;
@property (nonatomic, readwrite) NSURL *articleURL;

@end

@implementation THNArticle

- (instancetype)initWithTitle:(NSString *)title
                    thumbnail:(NSURL *)thumbnail
                     abstract:(NSString *)abstract
                    articleID:(NSNumber *)articleID
                   articleURL:(NSURL *)articleURL
{
    self = [self init];

    if (self)
    {
        _title = title;
        _thumbnail = thumbnail;
        _abstract = abstract;
        _articleID = articleID;
        _articleURL = articleURL;
    }

    return self;
}

@end
