//
//  THNURLConcierge.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNURLConcierge.h"

static NSString * currentBaseURL = @"http://gameofthrones.wikia.com/api/";
static NSString * currentAPIVersion = @"v1/";
static NSString * currentCharactersNews = @"Articles/Top?expand=1&category=characters&limit=75";

@implementation THNURLConcierge

- (NSURL *)baseURL
{
    return [NSURL URLWithString:currentBaseURL];
}

- (NSURL *)apiURL
{
    NSString *api = [currentBaseURL stringByAppendingString:currentAPIVersion];

    return [NSURL URLWithString:api];
}

- (NSURL *)charactersNewsURL
{
    NSString *news = [[currentBaseURL stringByAppendingString:currentAPIVersion] stringByAppendingString:currentCharactersNews];

    return [NSURL URLWithString:news];
}

@end
