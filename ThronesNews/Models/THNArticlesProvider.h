//
//  THNArticlesProvider.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "THNArticle.h"

@class THNArticlesProvider;

@protocol THNMainModelDelegate <NSObject>

- (void)mainModel:(THNArticlesProvider *)model didUpdateArticles:(NSArray <THNArticle *>*)articles;

@end

typedef NS_ENUM(NSInteger, THNMainModelState)
{
    THNMainModelStateAllArticles = 0,
    THNMainModelStateFavourites = 1
};

@interface THNArticlesProvider : NSObject

@property (nonatomic, weak) id <THNMainModelDelegate> delegate;

/*
 * State of currently returned data.
 * If not set, default value is NMainModelStateAllArticles.
 */
@property (nonatomic, assign) THNMainModelState state;

- (THNArticle *)articleAtIndexPath:(NSIndexPath *)path;

- (NSInteger)numberOfArticles;

// If article is already added, will be removed
- (void)favouriteArticleWithID:(NSNumber *)articleID;
- (BOOL)isFavouriteArticleID:(NSNumber *)articleID;

@end
