//
//  THNURLConcierge.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface THNURLConcierge : NSObject

- (NSURL *)baseURL;
- (NSURL *)apiURL;

/**
 * Characters News is a method that returns NSURL
 * used to get characters articles.
 * By default maximum number is 75.
 * You can change this limit in implementation of this class.
 * In future versions, this can be enriched by method
 * to set limit.
 *
 *  @return URL associated with characters endpoint.
 */
- (NSURL *)charactersNewsURL;

@end
