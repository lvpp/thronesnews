//
//  THNArticle.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface THNArticle : NSObject

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSURL *thumbnail;
@property (nonatomic, readonly) NSString *abstract;
@property (nonatomic, readonly) NSNumber *articleID;
@property (nonatomic, readonly) NSURL *articleURL;

- (instancetype)initWithTitle:(NSString *)title
                    thumbnail:(NSURL *)thumbnail
                     abstract:(NSString *)abstract
                    articleID:(NSNumber *)articleID
                   articleURL:(NSURL *)articleURL;

@end
