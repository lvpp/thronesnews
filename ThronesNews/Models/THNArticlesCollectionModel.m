//
//  THNArticlesCollectionModel.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNArticlesCollectionModel.h"
#import "THNArticlesProvider.h"
#import "THNArticleCollectionViewCell.h"

@interface THNArticlesCollectionModel () <THNArticleCollectionViewCellDelegate>

@property (nonatomic, strong) UICollectionView *articlesCollection;
@property (nonatomic) THNArticlesProvider *mainModel;

@end

@implementation THNArticlesCollectionModel

- (instancetype)initWithCollectionView:(UICollectionView *)articlesCollection
                         articlesModel:(THNArticlesProvider *)articlesModel
{
    self = [self init];

    if (self)
    {
        _articlesCollection = articlesCollection;
        UINib *cellNib = [UINib nibWithNibName:@"THNArticleCollectionViewCell" bundle:nil];
        [_articlesCollection registerNib:cellNib
                  forCellWithReuseIdentifier:@"ArticleCell"];
        _mainModel = articlesModel;
        _mainModel.delegate = self;
    }

    return self;
}

#pragma mark - UICollectionViewDataSource

- (THNArticleCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    THNArticleCollectionViewCell *cell = [self.articlesCollection dequeueReusableCellWithReuseIdentifier:@"ArticleCell" forIndexPath:indexPath];

    if (!cell) {
        cell = [[THNArticleCollectionViewCell alloc] init];
    }

    THNArticle *currentArticle = [self.mainModel articleAtIndexPath:indexPath];

    /* By injecting data to cell we can decouple ViewController from View
     * and later easier modify how we present data - without need
     * to change ViewController code.
     */
    [cell displayTitle:currentArticle.title
             thumbnail:currentArticle.thumbnail
              abstract:currentArticle.abstract
              forIndex:indexPath.item];
    cell.delegate = self;
    [cell setIsFavourite:[self.mainModel isFavouriteArticleID:currentArticle.articleID]];
    [cell showBorder];

    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.mainModel numberOfArticles];
}

#pragma mark - THNMainModelDelegate

- (void)mainModel:(THNArticlesProvider *)model didUpdateArticles:(NSArray<THNArticle *> *)articles
{   
    [self.articlesCollection reloadData];
}

#pragma mark - THNArticleCollectionViewCellDelegate

- (void)articleCollectionViewCell:(THNArticleCollectionViewCell *)cell
    userDidSelectFavouriteForItem:(NSInteger)item
{
    NSIndexPath *path = [NSIndexPath indexPathForItem:item inSection:0];
    NSNumber *articleID = [[self.mainModel articleAtIndexPath:path] articleID];
    [self.mainModel favouriteArticleWithID:articleID];
    [self.articlesCollection reloadData];
}

#pragma mark - THNMainViewControllerDelegate

- (void)mainViewController:(THNMainViewController *)viewController showFavourites:(UIButton *)sender
{    
    self.mainModel.state = THNMainModelStateFavourites;
    [self.articlesCollection reloadData];
}

- (void)mainViewController:(THNMainViewController *)viewController showAll:(UIButton *)sender
{
    self.mainModel.state = THNMainModelStateAllArticles;
    [self.articlesCollection reloadData];
}

@end
