//
//  THNArticlesCollectionModel.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "THNArticlesProvider.h"
#import "THNMainViewController.h"

@interface THNArticlesCollectionModel : NSObject <UICollectionViewDataSource, THNMainModelDelegate, THNMainViewControllerDelegate>

- (instancetype)initWithCollectionView:(UICollectionView *)articlesCollection
                         articlesModel:(THNArticlesProvider *)articlesModel;

@end
