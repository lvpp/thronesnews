//
//  THNAppDelegate.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface THNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

