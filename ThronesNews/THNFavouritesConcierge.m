//
// Created by Łukasz Pikor on 22.11.2015.
// Copyright (c) 2015 Lukasz Pikor. All rights reserved.
//

#import "THNFavouritesConcierge.h"

@interface THNFavouritesConcierge()

@property (nonatomic) NSMutableArray *favourites;

@end

static NSString * favouritesURL = @"/favourites.thn";

@implementation THNFavouritesConcierge

- (instancetype)init
{
    self = [super init];

    if (self)
    {
        [self read];
    }
    return self;
}

#pragma mark - Public

- (void)favouriteItem:(NSNumber *)itemID
{
    if ([self.favourites containsObject:itemID])
    {
        [self.favourites removeObject:itemID];
    }
    else
    {
        [self.favourites addObject:itemID];
    }

    [self save];
}

- (NSArray *)allIDs
{
    return [NSArray arrayWithArray:self.favourites];
}

- (BOOL)doesContainItem:(NSNumber *)itemID
{
    return [self.favourites containsObject:itemID];
}

#pragma mark - Private

- (void)read
{
    self.favourites = [NSMutableArray arrayWithContentsOfFile:[self favouritesPath]];

    if (!self.favourites)
    {
        self.favourites = [[NSMutableArray alloc] init];
    }
}

- (void)save
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.favourites writeToFile:[self favouritesPath] atomically:YES];
    });
}

- (NSString *)favouritesPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *favouritesPath = [documentsDirectory stringByAppendingPathComponent:favouritesURL];

    return favouritesPath;
}

@end
