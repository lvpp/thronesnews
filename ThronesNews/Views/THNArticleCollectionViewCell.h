//
//  THNArticleCollectionViewCell.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 22.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <UIKit/UIKit.h>

@class THNArticleCollectionViewCell;

@protocol THNArticleCollectionViewCellDelegate <NSObject>

- (void)articleCollectionViewCell:(THNArticleCollectionViewCell *)cell
    userDidSelectFavouriteForItem:(NSInteger)item;

@end

@interface THNArticleCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id <THNArticleCollectionViewCellDelegate> delegate;

- (void)displayTitle:(NSString *)title
           thumbnail:(NSURL *)url
            abstract:(NSString *)abstract
            forIndex:(NSInteger)index;

- (void)setIsFavourite:(BOOL)favourite;
- (void)showBorder;

@end
