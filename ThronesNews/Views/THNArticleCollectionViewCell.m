//
//  THNArticleCollectionViewCell.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 22.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNArticleCollectionViewCell.h"
@import WebImage;

@interface THNArticleCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UIView *thumbnailContainer;
@property (weak, nonatomic) IBOutlet UILabel *abstract;
@property (weak, nonatomic) IBOutlet UIButton *favourite;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation THNArticleCollectionViewCell

#pragma mark - Public

- (void)displayTitle:(NSString *)title
           thumbnail:(NSURL *)url
            abstract:(NSString *)abstract
            forIndex:(NSInteger)index
{
    self.title.text = title;

    [self.activityIndicator startAnimating];
    [self.thumbnail sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image && !error)
        {
            [self.activityIndicator stopAnimating];
        }
    }];
    self.abstract.text = abstract;
    self.favourite.tag = index;
}

- (void)setIsFavourite:(BOOL)favourite
{
    self.favourite.selected = favourite;
}

- (void)showBorder
{
    UIColor *bloodColor = [UIColor colorWithRed:0.71 green:0.157 blue:0.119 alpha:1];
    [self.layer setBorderColor:bloodColor.CGColor];
    [self.layer setBorderWidth:1.0f];
    [self.layer setCornerRadius:10.0f];
}

#pragma mark - Private

- (IBAction)favouriteButtonTouched:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(articleCollectionViewCell:userDidSelectFavouriteForItem:)])
    {
        [self.delegate articleCollectionViewCell:self
                   userDidSelectFavouriteForItem:sender.tag];
    }
}


@end
