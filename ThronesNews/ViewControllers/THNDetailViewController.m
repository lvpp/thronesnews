//
//  THNDetailViewController.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 24.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNDetailViewController.h"
@import WebImage;

@interface THNDetailViewController ()

@property (nonatomic, strong) UIBarButtonItem *favouritesButton;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnailView;
@property (nonatomic, weak) IBOutlet UILabel *abstractLabel;

@end

@implementation THNDetailViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setupUI];
}

#pragma mark - UI Interactions

- (void)favouritesButtonTouched:(UIButton *)sender
{
    [self.model favouriteArticleWithID:self.article.articleID];

    sender.selected = [self.model isFavouriteArticleID:self.article.articleID];
}

- (IBAction)readMoreButtonTouched:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:self.article.articleURL])
    {
        [[UIApplication sharedApplication] openURL:self.article.articleURL];
    }
}

#pragma mark - Private

- (void)setupUI
{
    [self createBarButtonItem];
    [self setupNavigationBar];
    [self displayData];
}

- (void)createBarButtonItem
{
    UIButton *actualButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 44)];

    [actualButton setImage:[UIImage imageNamed:@"star-outline"] forState:UIControlStateNormal];
    [actualButton setImage:[UIImage imageNamed:@"star"] forState:UIControlStateSelected];
    actualButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;

    [actualButton addTarget:self
                     action:@selector(favouritesButtonTouched:)
           forControlEvents:UIControlEventTouchUpInside];

    actualButton.selected = [self.model isFavouriteArticleID:self.article.articleID];

    self.favouritesButton = [[UIBarButtonItem alloc] initWithCustomView:actualButton];
    [self.navigationItem setRightBarButtonItem:self.favouritesButton];
}

- (void)setupNavigationBar
{
    self.navigationItem.title = self.article.title;
}

- (void)displayData
{
    // SDWebImage is handling caching for us.
    [self.thumbnailView sd_setImageWithURL:self.article.thumbnail];
    self.abstractLabel.text = self.article.abstract;
}

@end
