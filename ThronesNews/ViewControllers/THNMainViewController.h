//
//  THNMainViewController.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THNArticlesProvider.h"

@class THNMainViewController;

@protocol THNMainViewControllerDelegate <NSObject>

- (void)mainViewController:(THNMainViewController *)viewController
            showFavourites:(UIButton *)sender;

- (void)mainViewController:(THNMainViewController *)viewController
                   showAll:(UIButton *)sender;

@end

@interface THNMainViewController : UIViewController

@property (nonatomic, weak) id <THNMainViewControllerDelegate> delegate;
@property (nonatomic) THNArticlesProvider *model;

@end

