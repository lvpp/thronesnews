//
//  THNMainViewController.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import "THNMainViewController.h"
#import "THNArticlesCollectionModel.h"
#import "SDVersion.h"
#import "THNDetailViewController.h"

@interface THNMainViewController () <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

// UI
@property (nonatomic, weak) IBOutlet UICollectionView *articlesCollection;
@property (nonatomic, strong) UIBarButtonItem *favouritesButton;

// Model integration
@property (nonatomic) THNArticlesCollectionModel *collectionModel;

@end

@implementation THNMainViewController

#pragma mark - UIViewController Life Cycle


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setupUIConnections];
    [self setupUI];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    THNArticle *selectedArticle = [self.model articleAtIndexPath:indexPath];

    THNDetailViewController *detailsVC = [[THNDetailViewController alloc] initWithNibName:@"THNDetailViewController"
                                                                                   bundle:nil];
    detailsVC.article = selectedArticle;
    detailsVC.model = self.model;

    [self.navigationController pushViewController:detailsVC animated:YES];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DeviceSize device = [SDVersion deviceSize];
    if (device == Screen4inch)
    {
        return CGSizeMake(135, 286);
    }
    else if (device == Screen4Dot7inch)
    {
        return CGSizeMake(160, 286);
    }
    else if (device == Screen5Dot5inch)
    {
        return CGSizeMake(180, 286);
    }

    return CGSizeMake(170, 170); // default value from XIB view
}

#pragma mark - UI Interactions

- (void)favouritesButtonTouched:(UIButton *)sender
{
    sender.selected = !sender.selected;

    if (sender.selected)
    {
        if ([self.delegate respondsToSelector:@selector(mainViewController:showFavourites:)])
        {
            [self.delegate mainViewController:self showFavourites:sender];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(mainViewController:showAll:)])
        {
            [self.delegate mainViewController:self showAll:sender];
        }
    }
}

#pragma mark - Private

- (void)setupUIConnections
{
    self.collectionModel = [[THNArticlesCollectionModel alloc] initWithCollectionView:self.articlesCollection
                                                                        articlesModel:self.model];
    self.articlesCollection.delegate = self;
    self.articlesCollection.dataSource = self.collectionModel;
    self.delegate = self.collectionModel;
}

- (void)setupUI
{
    [self createBarButtonItem];
}

- (void)createBarButtonItem
{
    UIButton *actualButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 130, 44)];

    UIColor *buttonTextColor = [UIColor colorWithRed:0.71 green:0.157 blue:0.119 alpha:1];

    [actualButton setTitleColor:buttonTextColor forState:UIControlStateNormal];
    [actualButton setTitleColor:buttonTextColor forState:UIControlStateSelected];

    [actualButton setTitle:@"Show favourites" forState:UIControlStateNormal];
    [actualButton setTitle:@"Show all" forState:UIControlStateSelected];

    actualButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;

    [actualButton addTarget:self
                     action:@selector(favouritesButtonTouched:)
           forControlEvents:UIControlEventTouchUpInside];

    self.favouritesButton = [[UIBarButtonItem alloc] initWithCustomView:actualButton];
    self.favouritesButton.target = self;
    self.favouritesButton.action = @selector(favouritesButtonTouched:);
    [self.navigationItem setRightBarButtonItem:self.favouritesButton];
}

@end
