//
//  THNDetailViewController.h
//  ThronesNews
//
//  Created by Łukasz Pikor on 24.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THNArticle.h"
#import "THNArticlesProvider.h"

@interface THNDetailViewController : UIViewController

@property (nonatomic) THNArticle *article;
@property (nonatomic) THNArticlesProvider *model;

@end
