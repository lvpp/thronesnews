//
// Created by Łukasz Pikor on 22.11.2015.
// Copyright (c) 2015 Lukasz Pikor. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface THNFavouritesConcierge : NSObject

- (void)favouriteItem:(NSNumber *)itemID;
- (NSArray *)allIDs;
- (BOOL)doesContainItem:(NSNumber *)itemID;

@end
