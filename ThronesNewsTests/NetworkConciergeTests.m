//
//  NetworkConciergeTests.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "THNNetworkConcierge.h"

@interface NetworkConciergeTests : XCTestCase

@end

@implementation NetworkConciergeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSharedInstanceEqual
{
    // Given
    THNNetworkConcierge *properObject = [THNNetworkConcierge sharedInstance];

    // When
    id object = [THNNetworkConcierge sharedInstance];

    // Then

    XCTAssertNotNil(object);

    XCTAssertTrue([object isMemberOfClass:[THNNetworkConcierge class]]);

    /* We do not use XCTAssertEqualObjects here
     * because goal of this test is to check
     * if pointers points to the same address in memory
     */
    XCTAssertTrue(properObject == object);
}

- (void)testFakeSharedInstance
{
    // Given
    THNNetworkConcierge *properObject = [THNNetworkConcierge sharedInstance];

    // When
    id fake = [self fakeNetworkConcierge];

    // Then
    XCTAssertNotEqual(properObject, fake);
    XCTAssertFalse(properObject == fake);
}

- (void)testFetchingData
{
    // Given
    XCTestExpectation *fetchExpectation = [self expectationWithDescription:@"Test for fetching most popular articles"];

    // When
    [[THNNetworkConcierge sharedInstance] fetchMostPopularArticlesWithCompletion:^(NSArray *news) {

        // Then
        XCTAssertNotNil(news);
        XCTAssertEqual(news.count, 75);

        [fetchExpectation fulfill];
    }];

    [self waitForExpectationsWithTimeout:3.0 handler:nil];
}

#pragma mark - Private

- (THNNetworkConcierge *)fakeNetworkConcierge
{
    return [[THNNetworkConcierge alloc] init];
}

@end
