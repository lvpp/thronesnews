//
//  URLConciergeTests.m
//  ThronesNews
//
//  Created by Łukasz Pikor on 20.11.2015.
//  Copyright © 2015 Lukasz Pikor. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "THNURLConcierge.h"

@interface URLConciergeTests : XCTestCase

@property (nonatomic) THNURLConcierge *urlsModel;

@end

@implementation URLConciergeTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.urlsModel = [[THNURLConcierge alloc] init];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBaseURL
{
    // Given
    NSURL *properURL = [NSURL URLWithString:@"http://gameofthrones.wikia.com/api/"];

    // When
    NSURL *testedURL = [self.urlsModel baseURL];

    // Then
    XCTAssertEqualObjects(properURL, testedURL);
}

- (void)testAPIURL
{
    // Given
    NSURL *properURL = [NSURL URLWithString:@"http://gameofthrones.wikia.com/api/v1/"];

    // When
    NSURL *testedURL = [self.urlsModel apiURL];

    // Then
    XCTAssertEqualObjects(properURL, testedURL);
}

- (void)testCharactersNewsURL
{
    // Given
    NSURL *properURL = [NSURL URLWithString:@"http://gameofthrones.wikia.com/api/v1/Articles/Top?expand=1&category=characters&limit=75"];

    // When
    NSURL *testedURL = [self.urlsModel charactersNewsURL];

    // Then
    XCTAssertEqualObjects(properURL, testedURL);
}

@end
